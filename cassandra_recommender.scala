sc.stop
import com.datastax.spark.connector._, org.apache.spark.SparkContext, org.apache.spark.SparkContext._, org.apache.spark.SparkConf

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

import org.apache.spark.mllib.recommendation.ALS
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.apache.spark.mllib.recommendation.Rating


// Connection Konfiguration
val conf = new SparkConf(true).set("spark.cassandra.connection.host", "localhost")
val sc = new SparkContext(conf)

// Objecte fuer die einzelnen Tabellenzeilen
case class RatingsRow( userId: Int, productId: Int, rate: Double)
case class JobsRow( jobId: Int, userId: Int, numberOfRecos: Int)
case class ResultsRow( jobId: Int, userId: Int, productIds: String)

// Prozess der auf aktionen der jobs Tabelle reagiert
// while( 1 == 1) {
  
  val jobs_rdd = sc.cassandraTable[ JobsRow]( "recommender", "jobs")
  
  if( !jobs_rdd.isEmpty()) {
    
    val ratings_rdd = sc.cassandraTable[ RatingsRow]("recommender", "ratings")
    
    // Daten passend aufbereiten
    val ratings = ratings_rdd.map{ case RatingsRow(user,movie,rating) => Rating(user.toInt,movie.toInt,rating.toDouble)}
    
    // Modell erstellen und Trainieren
    val rank = 10
    val numIterations = 10
    val model = ALS.train( ratings, rank, numIterations, 0.01)
    
    // Ergebnis berechnen und in der tabelle results speichern
    val p = model.recommendProducts( jobs_rdd.first.userId, jobs_rdd.first.numberOfRecos)
    val collection = sc.parallelize( Seq( (  jobs_rdd.first.jobId,  jobs_rdd.first.userId, p.mkString( ", ")) ))
    collection.saveToCassandra( "recommender", "results", SomeColumns( "job_id", "user_id", "product_ids"))

    // Fertig bearbeiteten Job aus der jobs Tabelle loeschen
    val delete = s"DELETE FROM recommender.jobs where job_id=" + jobs_rdd.first.jobId + ";"
    val cluster = Cluster.builder().addContactPoint( "127.0.0.1").build()
    val session = cluster.connect()
    session.execute( delete)
    session.close
  }
// }